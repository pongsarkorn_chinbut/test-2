from django.shortcuts import redirect, render
from lists.models import Item
from sys import maxsize


def home_page(request):
    if request.method == 'POST':
        Item.objects.create(text=request.POST['item_text'],
                            money=float(request.POST['money_text']))
        return redirect('/')

    items = Item.objects.all()
    result = find_result()
    return render(request, 'home.html', {
        'items': items,
        'saved_money': format(result[0], '.2f'),
        'average_money': format(result[1], '.2f'),
        'max_saved': format(result[2], '.2f'),
        'min_saved': format(result[3], '.2f'),
                  })


def view_page_sort_by_time(request):
    items = Item.objects.all()
    result = find_result()
    return render(request, 'home.html', {
        'items': items,
        'saved_money': format(result[0], '.2f'),
        'average_money': format(result[1], '.2f'),
        'max_saved': format(result[2], '.2f'),
        'min_saved': format(result[3], '.2f'),
                  })


def min_to_max(request):
    if request.method == 'POST':
        return redirect('/lists/min_to_max')

    items = Item.objects.all().order_by('money')
    result = find_result()
    return render(request, 'home.html', {
        'items': items,
        'saved_money': format(result[0], '.2f'),
        'average_money': format(result[1], '.2f'),
        'max_saved': format(result[2], '.2f'),
        'min_saved': format(result[3], '.2f'),
                  })


def max_to_min(request):
    if request.method == 'POST':
        return redirect('/lists/max_to_min')

    items = Item.objects.all().order_by('-money')
    result = find_result()
    return render(request, 'home.html', {
        'items': items,
        'saved_money': format(result[0], '.2f'),
        'average_money': format(result[1], '.2f'),
        'max_saved': format(result[2], '.2f'),
        'min_saved': format(result[3], '.2f'),
                  })


def find_result():
    items = Item.objects.all()
    saved_money = 0
    max_saved = 0
    min_saved = maxsize
    item_number = Item.objects.count()
    for item in items:
        saved_money = saved_money + item.money
        if item.money < min_saved:  # find min
            min_saved = item.money
        if item.money > max_saved:  # find max
            max_saved = item.money

    if item_number == 0:
        average_money = 0
    else:
        average_money = saved_money/int(item_number)

    if min_saved == maxsize:
        min_saved = 0

    return [saved_money, average_money, max_saved, min_saved]
