from django.core.urlresolvers import resolve
from django.http import HttpRequest
from django.template.loader import render_to_string
from django.test import TestCase
from lists.models import Item
from lists.views import home_page, min_to_max, max_to_min
import pep8


class HomePageTest(TestCase):
    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    def test_home_page_returns_correct_html(self):
        request = HttpRequest()
        response = home_page(request)
        expected_html = render_to_string('home.html', {'saved_money': '0.00',
                                                       'average_money': '0.00',
                                                       'max_saved': '0.00',
                                                       'min_saved': '0.00', }
                                         )
        self.assertEqual(response.content.decode(), expected_html)

    def test_home_page_displays_all_list_items(self):
        Item.objects.create(text='item 1', money=10)
        Item.objects.create(text='item 2', money=20)

        request = HttpRequest()
        response = home_page(request)

        self.assertIn('item 1', response.content.decode())
        self.assertIn('10.0', response.content.decode())
        self.assertIn('item 2', response.content.decode())
        self.assertIn('20.0', response.content.decode())

    def test_home_page_can_save_a_POST_request(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['item_text'] = 'A new detail'
        request.POST['money_text'] = '100'

        response = home_page(request)

        self.assertEqual(Item.objects.count(), 1)
        new_item = Item.objects.first()
        self.assertEqual(new_item.text, 'A new detail')
        self.assertEqual(new_item.money, 100.0)

    def test_home_page_redirects_after_POST(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['item_text'] = 'A new detail'
        request.POST['money_text'] = '100'

        response = home_page(request)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

    def test_home_page_only_saves_items_when_necessary(self):
        request = HttpRequest()
        home_page(request)
        self.assertEqual(Item.objects.count(), 0)


class ItemModelTest(TestCase):
    def test_saving_and_retrieving_items(self):
        first_item = Item()
        first_item.text = 'The first'
        first_item.money = 10
        first_item.save()

        second_item = Item()
        second_item.text = 'The second'
        second_item.money = 20
        second_item.save()

        saved_items = Item.objects.all()
        self.assertEqual(saved_items.count(), 2)

        first_saved_item = saved_items[0]
        second_saved_item = saved_items[1]
        self.assertEqual(first_saved_item.text, 'The first')
        self.assertEqual(first_saved_item.money, 10)
        self.assertEqual(second_saved_item.text, 'The second')
        self.assertEqual(second_saved_item.money, 20)


class CalculateTest(TestCase):
    def test_calculate_correctly(self):
        Item.objects.create(text='item A', money=20)
        Item.objects.create(text='item B', money=60)

        request = HttpRequest()
        response = home_page(request)

        self.assertIn('80.00', response.content.decode())  # sum
        self.assertIn('40.00', response.content.decode())  # average
        self.assertIn('60.00', response.content.decode())  # max
        self.assertIn('20.00', response.content.decode())  # min


class SortingTest(TestCase):
    def test_sort_min_to_max(self):
        Item.objects.create(text='item 1', money=10)
        Item.objects.create(text='item 2', money=20)

        request = HttpRequest()
        response = min_to_max(request)

        saved_items = Item.objects.all().order_by('money')
        first_saved_item = saved_items[0]
        second_saved_item = saved_items[1]

        self.assertEqual(first_saved_item.text, 'item 1')
        self.assertEqual(first_saved_item.money, 10)
        self.assertEqual(second_saved_item.text, 'item 2')
        self.assertEqual(second_saved_item.money, 20)

    def test_sort_max_to_min(self):
        Item.objects.create(text='item 1', money=10)
        Item.objects.create(text='item 2', money=20)

        saved_items = Item.objects.all().order_by('-money')
        first_saved_item = saved_items[0]
        second_saved_item = saved_items[1]

        self.assertEqual(first_saved_item.text, 'item 2')
        self.assertEqual(first_saved_item.money, 20)
        self.assertEqual(second_saved_item.text, 'item 1')
        self.assertEqual(second_saved_item.money, 10)


class TestCodeFormat(TestCase):
    def test_pep8_conformance(self):
        # """Test that we conform to PEP8."""
        pep8style = pep8.StyleGuide(show_source=True)
        result = pep8style.check_files(paths=[
                        '/home/pongsarkorn/test2/superlists/lists/'])
        self.assertEqual(result.total_errors, 0,
                         "Found code style errors (and warnings).")
        result = pep8style.check_files([
                 '/home/pongsarkorn/test2/superlists/functional_tests.py',
                 '/home/pongsarkorn/test2/superlists/superlists/urls.py', ])
        self.assertEqual(result.total_errors, 0,
                         "Found code style errors (and warnings).")
