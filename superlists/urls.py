from django.conf.urls import patterns, include, url
from django.contrib import admin


urlpatterns = patterns('',
                       # Examples:
                       url(r'^$', 'lists.views.home_page', name='home'),
                       url(r'^lists/min_to_max$', 'lists.views.min_to_max',
                           name='min_to_max'),
                       url(r'^lists/max_to_min$', 'lists.views.max_to_min',
                           name='max_to_min'),
                       url(r'^lists/by_time$',
                           'lists.views.view_page_sort_by_time',
                           name='by_time'),
                       # url(r'^blog/', include('blog.urls')),

                       # url(r'^admin/', include(admin.site.urls)),
                       )
