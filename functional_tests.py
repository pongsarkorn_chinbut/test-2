from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest


class NewVisitorTest(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def check_for_row_in_list_table(self, row_text):
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])

    def check_for_row_in_result_table(self, row_text):
        table = self.browser.find_element_by_id('result_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])

    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get('http://localhost:8000')
        self.assertIn('Saving Money App', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Saving Money', header_text)

        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertEqual(
                inputbox.get_attribute('placeholder'),
                'Enter the Saving Detail'
        )

        inputbox.send_keys('from father')

        inputmoney = self.browser.find_element_by_id('id_new_money')
        self.assertEqual(
                inputmoney.get_attribute('placeholder'),
                'Enter money'
        )

        inputmoney.send_keys('50')

        self.browser.find_element_by_id('submit_data').click()
        self.check_for_row_in_list_table('from father 50.0 ฿')

        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('from mother')
        inputmoney = self.browser.find_element_by_id('id_new_money')
        inputmoney.send_keys('70')
        self.browser.find_element_by_id('submit_data').click()

        self.check_for_row_in_list_table('from mother 70.0 ฿')
        self.check_for_row_in_list_table('from father 50.0 ฿')
        self.check_for_row_in_result_table('TOTAL : 120.00 ฿')
        self.check_for_row_in_result_table('AVERAGE : 60.00 ฿')
        self.check_for_row_in_result_table('MAX : 70.00 ฿')
        self.check_for_row_in_result_table('MIN : 50.00 ฿')

        # เมื่อกด min to max จะยังมีข้อมูลเหมือนเดิม
        self.browser.find_element_by_id('min_to_max').click()
        self.check_for_row_in_list_table('from mother 70.0 ฿')
        self.check_for_row_in_list_table('from father 50.0 ฿')
        self.check_for_row_in_result_table('TOTAL : 120.00 ฿')
        self.check_for_row_in_result_table('AVERAGE : 60.00 ฿')
        self.check_for_row_in_result_table('MAX : 70.00 ฿')
        self.check_for_row_in_result_table('MIN : 50.00 ฿')

        # เมื่อกด max to min จะยังมีข้อมูลเหมือนเดิม
        self.browser.find_element_by_id('max_to_min').click()
        self.check_for_row_in_list_table('from mother 70.0 ฿')
        self.check_for_row_in_list_table('from father 50.0 ฿')
        self.check_for_row_in_result_table('TOTAL : 120.00 ฿')
        self.check_for_row_in_result_table('AVERAGE : 60.00 ฿')
        self.check_for_row_in_result_table('MAX : 70.00 ฿')
        self.check_for_row_in_result_table('MIN : 50.00 ฿')
        self.fail('Finish the test!')

if __name__ == '__main__':
    unittest.main(warnings='ignore')
